#include <iostream>
#include <map>
#include <chrono>

#include "InertialSense.h"
#define DEFAULT_BAUDRATE 3000000

std::map<int, int> msgCounter;

void getDeviceInfo(InertialSense &is) {
    dev_info_t res = is.GetDeviceInfo();
    std::cout << "===========================================================" << std::endl;
    std::cout << "serialNumber: " << res.serialNumber << std::endl;
    std::cout << "hardwareVer:  " << std::to_string(res.hardwareVer[0]) + "." +
                                     std::to_string(res.hardwareVer[1]) + "." +
                                     std::to_string(res.hardwareVer[2]) + "." +
                                     std::to_string(res.hardwareVer[3])
                                  << std::endl
    ;
    std::cout << "firmwareVer:  " << std::to_string(res.firmwareVer[0]) + "." +
                                     std::to_string(res.firmwareVer[1]) + "." +
                                     std::to_string(res.firmwareVer[2]) + "." +
                                     std::to_string(res.firmwareVer[3])
                                  << std::endl
    ;
    std::cout << "buildNumber:  " << res.buildNumber << std::endl;
    std::cout << "protocolVer:  " << std::to_string(res.protocolVer[0]) + "." +
                                     std::to_string(res.protocolVer[1]) + "." +
                                     std::to_string(res.protocolVer[2]) + "." +
                                     std::to_string(res.protocolVer[3])
                                  << std::endl
    ;
    std::cout << "repoRevision: " << res.repoRevision << std::endl;
    std::cout << "manufacturer: " << std::string(res.manufacturer) << std::endl;

    std::cout << "buildDate:    " << std::endl
              << "    status:   " << (res.buildDate[0] == 'r' ? "release" : "debug") << std::endl
              << "    year:     " << (unsigned int)res.buildDate[1] << std::endl
              << "    month:    " << (unsigned int)res.buildDate[2] << std::endl
              << "    day:      " << (unsigned int)res.buildDate[3] << std::endl
    ;

    std::cout << "buildtime:       " << std::endl
              << "    hour:        " << (unsigned int)res.buildTime[0] << std::endl
              << "    minute:      " << (unsigned int)res.buildTime[1] << std::endl
              << "    second:      " << (unsigned int)res.buildTime[2] << std::endl
              << "    millisecond: " << (unsigned int)res.buildTime[3] << std::endl
    ;

    std::cout << "addInfo:         " << std::string(res.addInfo) << std::endl;
    std::cout << "===========================================================" << std::endl << std::endl;

}

void subscribeMessages(InertialSense &is, bool skipInl2MagObsInfo = false) {
    auto data_callback = [=](void* i, p_data_t* data, int p_handle) {
        if (msgCounter.find((int)data->hdr.id) == msgCounter.end())
            msgCounter[(int)data->hdr.id] = 0;
        msgCounter[(int)data->hdr.id] += 1;
    };

    std::cout << "We are subscribing to multiple messages. ";
    if (skipInl2MagObsInfo)
        std::cout << "We are leaving out DID_INL2_MAG_OBS_INFO";
    std::cout << std::endl;

    is.BroadcastBinaryData(DID_SYS_SENSORS,       100, data_callback);
    is.BroadcastBinaryData(DID_SYS_PARAMS,        100, data_callback);
    
    if (!skipInl2MagObsInfo)
        is.BroadcastBinaryData(DID_INL2_MAG_OBS_INFO, 10,  data_callback);

    is.BroadcastBinaryData(DID_INL2_NED_SIGMA,    1,   data_callback);
    is.BroadcastBinaryData(DID_INS_2,             10,  data_callback);
    is.BroadcastBinaryData(DID_GPS1_POS,          100, data_callback);
    is.BroadcastBinaryData(DID_GPS2_RTK_CMP_REL,  10,  data_callback);
    is.BroadcastBinaryData(DID_BIT,               10,  data_callback);
    is.BroadcastBinaryData(DID_FLASH_CONFIG,      1,   data_callback);
    is.BroadcastBinaryData(DID_INFIELD_CAL,       100, data_callback);
}

std::string mapDids(int did) {
    switch (did) {
        case 4: 
            return "DID_INS_1";
        case 5: 
            return "DID_INS_2";
        case 10: 
            return "DID_SYS_PARAMS";
        case 11: 
            return "DID_SYS_SENSORS";
        case 12: 
            return "DID_FLASH_CONFIG";
        case 13: 
            return "DID_GPS1_POS";
        case 14: 
            return "DID_GPS2_POS";
        case 59: 
            return "DID_INL2_MAG_OBS_INFO";
        case 64: 
            return "DID_BIT";
        case 67: 
            return "DID_INL2_NED_SIGMA";
        case 91: 
            return "DID_GPS2_RTK_CMP_REL";
        case 94: 
            return "DID_INFIELD_CAL";
        default:
            return "UNKNOWN";
    };
}

int main() {
    // Initialization -- adjust this for your own system
    std::string port = "/dev/ttyUSB0";
    int         baud = IS_BAUD_RATE_BOOTLOADER;

    InertialSense is;
    
    ///// === Part One ========================================================
    // Try to open
    std::cout << "Trying to open " << port << " at baud " << baud << std::endl;
    if (!is.Open(port.c_str(), baud, true /* disable_broadcasts_on_close */)) {
        std::cout << "Failed to open port." << std::endl;
        return 1;
    }
    std::cout << "Success." << std::endl;

    getDeviceInfo(is);

    // Message settings
    nvm_flash_cfg_t res = is.GetFlashConfig();

    res.startupImuDtMs = 1;
    res.startupNavDtMs = 10;
    res.startupGPSDtMs = 200;

    is.SetFlashConfig(res);

    subscribeMessages(is);

    std::cout << "We will run 60 seconds and count the messages received." << std::endl;
    // Call update as fast as we can for 60 seconds
    auto start = std::chrono::steady_clock::now();
    while (true) {
        std::chrono::duration<double> diff = std::chrono::steady_clock::now() - start;
        if (diff.count() > 60) {
            break;
        }
        is.Update();
    }

    is.Close();

    std::cout << std::endl;
    for (auto entry: msgCounter) {
        std::cout << mapDids(entry.first) << ": " << entry.second << std::endl;
    }

    msgCounter.clear();
    
    ///// === Part Two ========================================================
    // Try to open
    std::cout << "Trying to open " << port << " at baud " << baud << std::endl;
    if (!is.Open(port.c_str(), baud, true /* disable_broadcasts_on_close */)) {
        std::cout << "Failed to open port." << std::endl;
        return 1;
    }
    std::cout << "Success." << std::endl;

    getDeviceInfo(is);

    // Message settings
    res = is.GetFlashConfig();

    res.startupImuDtMs = 1;
    res.startupNavDtMs = 10;
    res.startupGPSDtMs = 200;

    is.SetFlashConfig(res);

    subscribeMessages(is, true); // Ignore the DI causing trouble

    std::cout << "We will run 60 seconds and count the messages received." << std::endl;
    // Call update as fast as we can for 60 seconds
    start = std::chrono::steady_clock::now();
    while (true) {
        std::chrono::duration<double> diff = std::chrono::steady_clock::now() - start;
        if (diff.count() > 60) {
            break;
        }
        is.Update();
    }

    is.Close();

    for (auto entry: msgCounter) {
        std::cout << mapDids(entry.first) << ": " << entry.second << std::endl;
    }

    msgCounter.clear();

    return 0;
}