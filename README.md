# Possible Bug

```
[10:26:02]  orrinjelo ~/git/inertialsense-sandbox $ ./build/main
Trying to open /dev/ttyUSB0 at baud 921600
Success.
===========================================================
serialNumber: 31770
hardwareVer:  3.1.3.0
firmwareVer:  1.8.6.0
buildNumber:  1126
protocolVer:  1.3.96.9
repoRevision: 1536
manufacturer: Inertial Sense INC
buildDate:    
    status:   release
    year:     22
    month:    5
    day:      13
buildtime:       
    hour:        10
    minute:      19
    second:      13
    millisecond: 52
addInfo:         INL2
===========================================================

We are subscribing to multiple messages. 
We will run 60 seconds and count the messages received.

DID_INS_2: 1500
DID_SYS_PARAMS: 600
DID_SYS_SENSORS: 600
DID_FLASH_CONFIG: 2
DID_GPS1_POS: 3
DID_INL2_MAG_OBS_INFO: 5999
DID_INL2_NED_SIGMA: 14998
DID_INFIELD_CAL: 600
Trying to open /dev/ttyUSB0 at baud 921600
Success.
===========================================================
serialNumber: 31770
hardwareVer:  3.1.3.0
firmwareVer:  1.8.6.0
buildNumber:  1126
protocolVer:  1.3.96.9
repoRevision: 1536
manufacturer: Inertial Sense INC
buildDate:    
    status:   release
    year:     22
    month:    5
    day:      13
buildtime:       
    hour:        10
    minute:      19
    second:      13
    millisecond: 52
addInfo:         INL2
===========================================================

We are subscribing to multiple messages. We are leaving out DID_INL2_MAG_OBS_INFO
We will run 60 seconds and count the messages received.
DID_INS_2: 160
DID_SYS_PARAMS: 149
DID_SYS_SENSORS: 106
DID_FLASH_CONFIG: 13569
DID_GPS1_POS: 1
DID_INL2_NED_SIGMA: 2896
DID_INFIELD_CAL: 135
```

|        Message        | # w/ all msgs | # w/ no mag_obs_info |
|-----------------------|---------------|----------------------|
| DID_INS_2             |  1500         |  160                 |
| DID_SYS_PARAMS        |  600          |  149                 |
| DID_SYS_SENSORS       |  600          |  106                 |
| DID_FLASH_CONFIG      |  2            |  13569               |
| DID_GPS1_POS          |  3            |  1                   |
| DID_INL2_MAG_OBS_INFO |  5999         |  DISABLED            |
| DID_INL2_NED_SIGMA    |  14998        |  2896                |
| DID_INFIELD_CAL       |  600          |  135                 |

* Why did DID_INS_2, DID_INL2_NED_SIMGA, DID_INFIELD_CAL decrease? If we reaching a bandwidth limit here, what determines the priority of messages?

* Why does enabling DID_INL2_MAG_OBS_INFO cause there to be such an extreme decrease in DID_FLASH_CONFIG messages, no matter what the period multiple is set to?
